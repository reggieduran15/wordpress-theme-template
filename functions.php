<?php
    
    // FEATURED IMAGE
    add_theme_support('post-thumbnails');
    
    // MAIN MENU
    function register_my_menu() {
        register_nav_menu( 'primary', __( 'Main Menu', 'theme-slug' ) );
    }

    add_action( 'after_setup_theme', 'register_my_menu' );

    if( function_exists('acf_add_options_page') ) {
		acf_add_options_page('General Settings');
	}

