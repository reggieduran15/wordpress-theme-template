if (jQuery) { $ = jQuery; }
if ($) { jQuery = $; }

var canBeLoaded = true;

$(document).ready(function() {
	$('.blog_loadmore').click(function(e){
 		e.preventDefault();
 		var $offset = $(this).data('offset');
		var button = $(this);
		var data = {
			'action': 'loadmore',
			'query': blog_loadmore_params.posts,
			'page' : blog_loadmore_params.current_page
		};
 
		$.ajax({ // you can also use $.post here
			url : blog_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			offset : $offset,
			type : 'POST',
			beforeSend : function ( xhr ) {				
				button.text('Loading...');
			},
			success : function( response ){
				button.data('offset', parseInt(response.offset));
				if( response.success ) {
					if(response.data.post.length > 0){
						var html = '';
						$(response.data.post).each(function(i, v){

							html += '<div class="item--column item--column-3 show" data-cat="'+v.post_cat+'">';
							html += '<a href="'+v.permalink+'">'
							html += '<div class="image--box">'
							html += '<img src="'+v.post_image+'" width="411" height="auto" alt="" title="" class="blog-thumb">'
							html += '</div>'

							html += '<div class="desc--overlay">'
							html += '<div class="desc--content">'
							html += '<h5>'+v.post_title+'</h5>'
							html += '<p class="author-date"><span>Be '+v.post_author+'</span>  <span class="sep">|</span>  <span>'+v.post_time+'</span></p>'
							html += '<p>'+v.post_excerpt+'</p>'
							html += '</div>'
							html += '</div>'
							html += '</a>'
							html += '</div>'
						});
						
						button.text( 'Load More' ).prev().before(data);

						$('#list--blog').append(html);
						button.show();

						if (response.data.post.length < 9) {
							button.hide();
						}

					} else {
                        button.hide();
                    }

                    canBeLoaded = true;

				} else {
					// do nothing
				}

				blog_loadmore_params.current_page++;
			}
		});
	});
});