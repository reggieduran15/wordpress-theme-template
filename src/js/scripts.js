
if (jQuery) { $ = jQuery; }
if ($) { jQuery = $; }


$(window).on('load',function(){

});


$(document).ready(function(){
	sliderMain();
});

// FUNCTION
function navToggle() {
	$('.menu-hamburger').on('click',function(e) {
		e.preventDefault();
		
		if ( $('body').hasClass('active-header_js') ) {
			$('body').removeClass('active-header_js');
			
			setTimeout(function(){
				$('body').removeClass('no-scroll_js');
			},1000);

		} else {
			$('body').addClass('active-header_js');
			$('body').addClass('no-scroll_js');
		}
	});
}

function initPageAnim(container) {
	$("body").find(".animated").appear(
		function () {
			var element = $(this);
			var animation = element.data("animation");
			var animationDelay = element.data("delay");

			if (animationDelay) {
				setTimeout(function () {
					element.addClass(animation);
					element.removeClass("hiding");
				}, animationDelay);
			} else {
				element.addClass(animation);
				element.removeClass("hiding");
			}
		}, { accY: -150 }
	);
}

function parallax() {
	if ($('.banner--bg').length) {
		parallaxBackground($('.banner--bg'), 0.1);
	};
}

function parallaxBackground(imgDiv,multiplier) {
	var wH = window.innerHeight ? window.innerHeight : $(window).height();
	var imgPar = imgDiv;

	var imgParPercY = (imgPar.offset().top - $(window).scrollTop()) / imgPar.outerHeight(false);

	var parallaxHeight = imgPar.outerHeight(false);
	var newH = Math.round((imgParPercY) * parallaxHeight * multiplier / 2);	
	TweenMax.to($(imgDiv), .4, {top: newH+'px', ease:Power1.easeOut});
}